﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NavalWar.DAL
{
    public class Player
    {
        [Key]
        public int _id { get; set; }
        
        
        
        public string _name { get; set; }

        public int SessionId { get; set; }

        public Session Session { get; set; }

    }
}
