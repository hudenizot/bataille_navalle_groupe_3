﻿using NavalWar.DTO;
using NavalWar.DTO.WebDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace NavalWar.DAL
{
    public static class Extension
    {
        public static NavalWar.DTO.Player toDTO(this Player j){
            return new NavalWar.DTO.Player() { _id=j._id,_name=j._name};
        }
    }
}
