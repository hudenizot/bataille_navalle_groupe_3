﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NavalWar.DAL
{
    public class NavalContext : DbContext
    {
        public NavalContext(DbContextOptions<NavalContext> options):base(options) {
        }

        public DbSet<Player> Joueurs { get; set; }
        public DbSet<Session> Sessions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Player>()
                .ToTable("Player")
                .HasOne(b => b.Session);

            modelBuilder.Entity<Session>()
                .ToTable("Session")
                .HasMany(p=>p.joueurs);
            

        }
    }
}
