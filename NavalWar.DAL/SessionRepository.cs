﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NavalWar.DTO;
using NavalWar.DTO.WebDTO;

namespace NavalWar.DAL
{
    public class SessionRepository :  ISessionRepository
    {
        private readonly NavalContext _context;

        public SessionRepository(NavalContext context)
        {
            _context = context;
        }

        public NavalWar.DTO.SessionDTO GetSession(int id)
        {
            try
            {
                Session session = _context.Sessions.FirstOrDefault(x => x.ID == id);
                SessionDTO s = new SessionDTO { id= session.ID };
                foreach(Player p in session.joueurs)
                {
                    PlayerDTO po= new PlayerDTO() {_id=p._id,_name=p._name };
                    s.joueurs.Add(po);
                }
                return s;
            }
            catch(Exception) {
                throw;
            }
        }
    }
}
