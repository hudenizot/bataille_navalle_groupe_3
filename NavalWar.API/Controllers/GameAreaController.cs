﻿using Microsoft.AspNetCore.Mvc;
using NavalWar.DTO;
using NavalWar.DTO.Area;
using NavalWar.DTO.WebDTO;
using NavalWar.Utils;
using NavalWar.Business;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NavalWar.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameAreaController : ControllerBase
    {
        private GameArea _area = new GameArea();
        private readonly IGameService _gameService;

        public GameAreaController(IGameService gameService)
        {
            _gameService = gameService;
        }
        // GET: api/<GameAreaController>
        [HttpGet("affichage grille")]
        public IActionResult GetPlayerBoards()
        {
            /*_area.AddPLayer();
            List<PlayerDTO> dto = new List<PlayerDTO>();
            foreach (var player in _area.Players)
            {
                dto.Add(new PlayerDTO
                {   
                    ShipBoard = player.GetPersonalBoard.ToListArray(),
                    ShotBoard = player.GetShotsBoard.ToListArray()
                });
            }
            */
            List<PlayerDTO> dto = _gameService.listPlayers();
            return Ok(dto);
        }


        // GET api/<GameAreaController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }
        
        //post du lieu du tir  , retour du resutat
        [HttpPut("Tirer")]
        public string GetCaseTir(int posY, int posX)
        {
            Player p2 = new Player();
            Player p1 = new Player();
            string result = p1.tirer(posY, posX, p2);
            return result;
        }




        // POST api/<GameAreaController>
        [HttpPost("Création Player")]
        public void Post([FromBody] string value)
        {

        }

        // PUT api/<GameAreaController>/5
        [HttpPut("Modification Player")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<GameAreaController>/5
        [HttpDelete("Destruction player")]
        public void Delete(int id)
        {
        }

    }
}
