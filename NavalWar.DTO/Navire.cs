﻿using System.Drawing;

namespace NavalWar.DTO
{
    public class Navire
    {
        private static int _compteur=0;
        
        public Navire(int h, int l)
        {
            _hauteur=h;
            _largeur=l;
            ++_compteur;
            _id = "nav" + _compteur;
            if (h==1) _vie = l;  ///vie = max de h et l
            else  _vie = h; 
            
        }

        public int _hauteur { get; set; }
        public int _largeur { get; set; }
        public int _vie { get; set; }
        public string _id { get; set; }

        public Point _position { get; set; }
    }
}